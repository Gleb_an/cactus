package com.cactus.object;

import java.util.List;

public class OrderObject {

    private String id;
    private int hostel;
    private int room;
    private String name;
    private String phone;
    private String location;
    private List<String> description;



    public OrderObject(){}

    public OrderObject(int hostel, int room, String name, String phone, String location, List<String> description) {
        setHostel(hostel);
        setRoom(room);
        setName(name);
        setPhone(phone);
        setLocation(location);
        setDescription(description);
        setId(phone);
    }


    public int getHostel() {
        return hostel;
    }

    public void setHostel(int hostel) {
        this.hostel = hostel;
    }

    public int getRoom() {
        return room;
    }

    public void setRoom(int room) {
        this.room = room;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public List getDescription() {
        return description;
    }

    public void setDescription(List<String> description) {
        this.description = description;
    }

    public String getId(){
        return this.id;
    }

    public void setId(String  id){
        this.id = id;
    }

    @Override
    public String toString() {
        return "OrderObject{" +
                "id='" + id + '\'' +
                ", hostel=" + hostel +
                ", room=" + room +
                ", name='" + name + '\'' +
                ", phone='" + phone + '\'' +
                ", location='" + location + '\'' +
                ", description=" + description +
                '}';
    }
}
