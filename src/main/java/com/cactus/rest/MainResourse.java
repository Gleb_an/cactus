package com.cactus.rest;

import com.cactus.elastic.ElasticOrderImpl;
import com.cactus.object.OrderObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Arrays;
import java.util.List;

/**
 * Created by gleb on 15.05.16.
 */
@Path("/")
@Controller
public class MainResourse {

    @Autowired
    private ElasticOrderImpl order;

    private String startHTML = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \n" +
            "  \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n" +
            "<html xmlns=\"http://www.w3.org/1999/xhtml\">\n" +
            " <head>\n" +
            " <link rel=\"stylesheet\" type=\"text/css\" src=\"/css/style.css\">\n" +
            "  <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n" +
            "  <title>Три колонки</title>\n" +
            " </head>\n" +
            " <body>\n" +
            "  <div class=\"layout\">";

    private String endHTML = "</body>\n" +
            "</html>";

    private String viewOrder = "<div class=\"col%d\">\n" +
            "   <t4>%s</t4>\n" +
            "    <table >\n" +
            "   <tr>\n" +
            "     <td align=\"center\">%s</td>\n" +
            "   </tr>\n" +
            "   <tr>\n" +
            "     <td align=\"center\">%s</td>\n" +
            "\t </tr>\n" +
            "\t <tr>\n" +
            "     <td align=\"center\">%s</td>\n" +
            "   </tr>\n" +
            "  </table> \n" +
            "</div>\n";


    @GET
    @Produces(MediaType.TEXT_HTML)
    public Response getAll() {
        List<OrderObject> list = order.getAll();
        StringBuilder builder = new StringBuilder();
        builder.append(startHTML);
        for (int i = 0; i < 3; i++) {
            OrderObject order = list.get(i);
            String s = String.format(viewOrder, i + 1, "Общежитие №" + order.getHostel(), Arrays.toString(order.getDescription().toArray()), order.getPhone(), order.getLocation());
            builder.append(s);
        }
        builder.append(endHTML);
        return Response.ok().entity(builder.toString()).build();
    }


}
