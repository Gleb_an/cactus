package com.cactus;

import com.cactus.elastic.ElasticOrderImpl;
import com.cactus.object.OrderObject;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class Main {

    public static void main(String[] args) {
        List<OrderObject> orderObjects = new ArrayList<OrderObject>();

        List<String> descriptions = new ArrayList<String>();
        descriptions.add("Батон");
        descriptions.add("Колбаса");
        descriptions.add("Пиво");

        orderObjects.add(new OrderObject(6, 518, "Sergey", "0930198191", "Silpo", descriptions));

//        Map<String, List> map = new HashMap<String, List>();
//
//
//        map.put(orderObjects.get(orderObjects.size()).getId(), orderObjects.get(orderObjects.size()).getDescription());

        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("WEB-INF/application.xml");

        ElasticOrderImpl order = applicationContext.getBean(ElasticOrderImpl.class);

        order.create(orderObjects.get(0));

        List<OrderObject> list = order.getAll();
        for (OrderObject object : list) {
            System.out.println(object);
        }

    }

}
