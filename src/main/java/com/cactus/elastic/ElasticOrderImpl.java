package com.cactus.elastic;

import com.cactus.object.OrderObject;
import org.codehaus.jackson.map.ObjectMapper;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.index.query.MatchAllQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by gleb on 15.05.16.
 */
@Repository
public class ElasticOrderImpl {

    private ObjectMapper objectMapper;

    @Autowired
    private ElasticsearchClient client;

    @PostConstruct
    public void init() {
        objectMapper = new ObjectMapper();
    }


    public void create(OrderObject orderObject) {
        try {
            String uuid = UUID.randomUUID().toString();
            orderObject.setId(uuid);
            String json = objectMapper.writeValueAsString(orderObject);
            client.getClient().prepareIndex(client.getIndex(), "order", orderObject.getId()).setSource(json).execute().actionGet();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<OrderObject> getAll() {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        SearchRequestBuilder builder = client.getClient().prepareSearch(client.getIndex()).setTypes("order")
                .setQuery(QueryBuilders.matchAllQuery()).setSearchType(SearchType.DFS_QUERY_AND_FETCH);
        MatchAllQueryBuilder queryBuilder = QueryBuilders.matchAllQuery();
        builder = builder.setQuery(queryBuilder);
        SearchResponse response = builder.setExplain(true).execute().actionGet();

        List<OrderObject> result = new ArrayList<OrderObject>();
        try {
            for (SearchHit hit : response.getHits().hits()) {
                String source = hit.sourceAsString();
                OrderObject dbUser = null;

                dbUser = objectMapper.readValue(source, OrderObject.class);

                result.add(dbUser);
            }
            return result;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new ArrayList<OrderObject>();
    }

}
