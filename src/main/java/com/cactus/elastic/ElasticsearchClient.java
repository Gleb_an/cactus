package com.cactus.elastic;

import org.apache.log4j.Logger;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Created by gleb on 15.05.16.
 */
@Repository
public class ElasticsearchClient {
    private static final Logger log = Logger.getLogger(ElasticsearchClient.class);

    private String clusteName = "elasticsearch";

    private int port = 9300;

    private String host = "localhost";

    private String index = "cactus";

    private Client client;

    @PostConstruct
    public void init() {
        try {
            log.debug("Initializing Elasticsearch client. cluster name is " + index);
            Settings settings = Settings.settingsBuilder()
                    .put("cluster.name", clusteName).build();
            client = TransportClient.builder().settings(settings).build()
                    .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName(host), port));
        } catch (UnknownHostException e) {
            log.error("Elasticsearch not initializing client. cluster name is " + index);
            e.printStackTrace();
        }
    }

    public Client getClient() {
        return client;
    }

    public String getIndex() {
        return index;
    }
}
